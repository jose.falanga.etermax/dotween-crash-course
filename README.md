# DOTWeen Crash Course

## What is DOTween
> DOTween is a fast, efficient, fully type-safe object-oriented animation engine for Unity, optimized for C# users, free and open-source, with tons of advanced features

The free version is more aimed to programmers. There is a Pro (paid) version with everything you have for free, but adds more component based tools and is aimed to non-programmers. This paid version also has extension methods for animating TextMeshPro components.

## Why DOTween

It has all the six P's of great game developing tools:
- Performant
- Powerful
- Popular (tried and tested by lots of people and projects, from AAA to your little indie game)
- PFree
- [PWellDocumented](http://dotween.demigiant.com/documentation.php)

## Overview

Every animation plays itself from the moment is created. You can also `await` them with Tasks, UniTask, or Observables, if the need arises.

### Shortcut Tweens:
Extension methods to animate almost everything related to Unity components. The general signature of such extensions is something like `componentInstance.DOUsefulStuff(targetValue,time)`

Some notable and very useful and common shortcuts:
- `material.DOColor(targetColor,time);` changes the color of a material to the target, gradually over time
- `canvasGroup.DOFade(targetAlphaLevel,time);` changes the value of the Alpha of a canvas group, also works for colors
- `transform.DOPunchScale(targetScale,time);` changes the scale of the object over time, to a the targetScale fastly, and then returns to the starting value, with a rubber-like of spring-like effect
- `transform.DORotate(targetRotation, time);` rotates the transform to a desired target
- `transform.DOJump(finalPosition,time);` *(yes, this makes the whole jump up movement with a parabolic curve and lands the transform in the place you want it to be after jumping, pretty cool)*

For a full list of the shortcuts provided, you can go to - [the documentation](http://dotween.demigiant.com/documentation.php). It's truly very complete. Do yourself a favor and check out `DOPath`, it supports bezier curves ;)

### Generic Tweens
The shortcuts cover probably 99% of your everyday use cases, but in rare occassions, depending on your domain, may fall short. For every animation needed uncovered by the shortcuts, you can use a generic tweens.

You can animate **any** value. The general usage is `DOTween.To(getter, setter, to, duration)` where the `getter` is a delegate to get the current value, and `setter` is a delegate called to set the value, `to` is the destination, and `duration` explains itself.

Examples:

`DOTween.To(() => this.someFloat, value => this.someFloat = value, 3, .5f)` animates the property `someFloat` of your class to `3` in `half a second` of time.

`DOTween.To(() => someString, s => someString = s, "something else than empty", 2.5f);` animates the property `someString` of your class to `something else than empty` in `two and half seconds` of time. *Animating a string means assigning the values progressively, making a typewritter-like effect.*
### Sequences:
A way to chain and mix other animation or callbacks (or sequences) like a **tree**. It has several operations: `Appending`, `Joining`, `Inserting` and `Prepending`. By far the most used operations are **`Appending`** and **`Joining`**, there is little you cannot do with those two.
#### Appending:
- `Append`: animate something **at the end** of the sequence
- `AppendInterval`: wait time **at the end** of the sequence
- `AppendCallback`: execute code **at the end** of the sequence
#### Inserting:
- `Insert`: animate something **at an arbitrary time** in a sequence timeline
- `InsertCallback`: execute code **at an arbitrary time** in a sequence timeline
#### Prepending:
- `Prepend`: animate something **at the begining** of the sequence
- `PrependInterval`: wait time **at the begining** of the sequence
- `PrependCallback`: execute code **at the begining** of the sequence
#### Joining:
- `Join`: Inserts the given tween **at the same time position** of the last tween or callback added to the Sequence.

### Loops
By default, animations and sequences don't loop. If you need, you can use the following extension:
- `.SetLoops(-1)` for `infinity` loops
- `.SetLoops(x)` for `x` amount loops

The `.SetLoops` extension method also receives optionally the type of loop to use:
- `LoopType.Restart`: Each loop cycle restarts from the beginning
- `LoopType.Yoyo`: The animation moves forward and backwards at alternate cycles
- `LoopType.Incremental`: Continuously increments the animation at the end of each loop cycle (A to B, B to B+(A-B), and so on), thus always moving "onward"

### Easings
- Out of the box support for most popular ones (like the ones in [easings.net](http://easings.net))
- By default, every animation uses `OutQuad`
- Can pass custom easings from an AnimationCurve

## Related Material
[Juice it or lose it](https://www.youtube.com/watch?v=Fy0aCDmgnxg)
[Slides to the Talk](https://docs.google.com/presentation/d/1MrJSrYbsowOjk2OY--WMSjob3RCG3NUtZa9qHt6jG2I/edit?usp=sharing)

using DG.Tweening;
using UnityEngine;

public class EasedMovingObject : MonoBehaviour
{
    public Ease ease;
    public float duration = 1;
    public float overshoot = 1.7f;

    public void Move()
    {
        transform.DOMoveX(-transform.position.x, duration)
            .SetEase(ease, overshoot);
    }
}
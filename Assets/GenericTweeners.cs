﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GenericTweeners : MonoBehaviour
{
    public float duration = 2;
    public Text genericString;
    [Space(100)]
    [Range(0,10)]
    public float myFloat = 0;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(DoThings);
    }

    private void DoThings()
    {
        DOTween.To(() => myFloat, x => myFloat = x, 10f, duration);
        DOTween.To(() => genericString.text, x => genericString.text = x, "something else than empty", duration);
    }
}

﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Rotate : MonoBehaviour
{
    public GameObject target;
    public Vector3 rotation;
    public float duration;

    public void DoThing()
    {
        target.transform.DORotate(rotation, duration, RotateMode.FastBeyond360);
    }
    
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(DoThing);
    }
}

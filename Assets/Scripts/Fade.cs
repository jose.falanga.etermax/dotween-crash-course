﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public GameObject target;
    [Range(0,1)]
    public float alpha;
    public float duration;

    public void DoThing()
    {
        target.GetComponent<Renderer>().material.DOFade(alpha, duration);
    }

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(DoThing);
    }
}

﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Punch : MonoBehaviour
{
    public GameObject target;
    public Vector3 punch;
    public float duration;

    public void DoThing()
    {
        target.transform.DOPunchScale(punch, duration);
    }
    
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(DoThing);
    }
}

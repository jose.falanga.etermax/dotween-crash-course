﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    public GameObject target;
    public Vector3 position;
    public float duration;

    public void DoThing()
    {
        target.transform.DOMove(position, duration);
    }
    
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(DoThing);
    }
}

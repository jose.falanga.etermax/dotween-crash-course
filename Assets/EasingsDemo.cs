﻿using UnityEngine;
using UnityEngine.UI;

public class EasingsDemo : MonoBehaviour
{
    public EasedMovingObject[] objects;
    
    private void Start() => 
        GetComponent<Button>().onClick.AddListener(MoveObjects);

    void MoveObjects()
    {
        foreach (var movingObject in objects) movingObject.Move();
    }
}